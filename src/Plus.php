<?php

declare(strict_types=1);

namespace DKX\Lib;

final class Plus
{
	public function calculate(int $a, int $b) : int
	{
		return $a + $b;
	}
}
