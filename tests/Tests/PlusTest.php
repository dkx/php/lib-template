<?php

declare(strict_types=1);

namespace DKXTests\Lib\Tests;

use DKX\Lib\Plus;
use DKXTests\Lib\TestCase;

final class PlusTest extends TestCase
{
	public function testCalculate() : void
	{
		$plus = new Plus();
		self::assertSame(3, $plus->calculate(1, 2));
	}
}
